$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "email_marketing/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "email_marketing"
  s.version     = EmailMarketing::VERSION
  s.authors     = ["ERick"]
  s.email       = ["erick_8911@hotmail.com"]
  s.homepage    = "http://www.boltech.mx"
  s.summary     = ""
  s.description = ""

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails"
  s.add_dependency "mailgun"
  s.add_dependency "marketing_connection"

end
