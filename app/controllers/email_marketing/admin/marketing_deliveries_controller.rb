module EmailMarketing
  module Admin
    class MarketingDeliveriesController < AdminsController
      before_action :set_values, only: [:new, :create]

      def index
        response = marketing_connection.deliveries.list
        @deliveries = response["deliveries"]
      end

      def new
        @delivery = Delivery.new
      end

      def create
        got_params = params.require(:admin_delivery).permit(:name, :domain, :gun_name, :template_id, :from, :list_id)
        @delivery = Delivery.new(got_params)
        @delivery.domain = @domains.first["name"] if @domains.count == 1
        response = @delivery.submit
        errors = response["errors"]
        if errors
          set_errors(@delivery, errors, {template: "template_id", list: "list_id"})
          render "new"
        else
          redirect_to admin_marketing_deliveries_path, flash: {success: "Enviado"}
        end
      end

      private

        def set_values
          response = marketing_connection.lists.list
          @lists = response["lists"]
          response = marketing_connection.templates.list
          @templates = response["templates"]
          response = marketing_connection.guns.list
          guns = response["guns"]
          mailgun = Mailgun::Base.new(api_key: guns.first["key"])
          @domains = mailgun.domains.list
        end

    end


    class Delivery < AdminsController
      include ActiveModel::Model
      attr_accessor :name, :template_id, :from, :list_id, :domain, :gun, :gun_name
      def submit
        json = JSON(self.to_json)
        parameters =  {delivery: json}
        response = marketing_connection.deliveries.create(parameters)
      end
    end

  end
end
